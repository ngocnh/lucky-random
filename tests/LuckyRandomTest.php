<?php

use Ngocnh\LuckyRandom\LuckyRandom;

class ChatworkBaseTest extends PHPUnit_Framework_TestCase
{
    public function testConstructor()
    {
        $luckyRandom = new LuckyRandom();
        $this->assertEquals(get_class($luckyRandom), 'Ngocnh\LuckyRandom\LuckyRandom');
    }

    public function testAddElement()
    {
        $originalData = [
            1 => 10,
            2 => 30,
            'three' => 50,
        ];
        $luckyRandom = new LuckyRandom();
        foreach ($originalData as $key => $value) {
            $luckyRandom->addElement($key, $value);
        }

        $data = $luckyRandom->getData();
        $this->assertEquals($originalData, $data);
    }

    public function removeElement()
    {
        $originalData = [
            1 => 10,
            2 => 30,
            'three' => 50,
        ];
        $luckyRandom = new LuckyRandom();
        $luckyRandom->setData($originalData);
        $luckyRandom->removeElement(1);
        $data = $luckyRandom->getData();
        unset($originalData[1]);
        $this->assertEquals($originalData, $data);
    }

    public function testRandom()
    {
        $originalData = [
            1 => 10,
            2 => 30,
            'three' => 50,
        ];
        $luckyRandom = new LuckyRandom();
        $luckyRandom->setData($originalData);

        $random = $luckyRandom->random();
        $this->assertCount(1, $random);
        foreach($random as $key) {
            $this->assertArrayHasKey($key, $originalData);
        }

        $random = $luckyRandom->random(2);
        $this->assertCount(2, $random);
        foreach($random as $key) {
            $this->assertArrayHasKey($key, $originalData);
        }

        $random = $luckyRandom->random(3);
        $this->assertCount(3, $random);
        foreach($random as $key) {
            $this->assertArrayHasKey($key, $originalData);
        }

        $random = $luckyRandom->random(4);
        $this->assertCount(3, $random);
        foreach($random as $key) {
            $this->assertArrayHasKey($key, $originalData);
        }
    }
}