LuckyRandom for PHP
==========

## Requirement
* PHP >= 5.4

## Install

You can install and manage LuckyRandom by using `Composer`

```
composer require ngocnh/lucky-random
```

Or add `ngocnh/lucky-random` into the require section of your `composer.json` file then run `composer update`

## Usage

```php
$luckyRandom = new NgocNH/LuckyRandom/LuckyRandom();
$data = [
    'ngocnh' => 10, 
];
$luckyRandom->setData($data);
$luckyRandom->addElement('Nguyen', 20);
$luckyRandom->addElement('Hong', 30);
$luckyRandom->addElement('Ngoc', 40);

// Random one element with weight.
$luckyRandom->random();

// Random two elements
$luckyRandom->random(2);
```

## Test
Just run `phpunit` to start test.